export interface MovieLite {
  movieId: number;
  caption: string;
  avatar: string;
  selected?: boolean;
}
